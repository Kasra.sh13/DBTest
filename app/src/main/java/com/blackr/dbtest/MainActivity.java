package com.blackr.dbtest;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.tv);

        SQLiteDatabase mydb = openOrCreateDatabase("testdb",MODE_PRIVATE,null);

        mydb.execSQL("CREATE TABLE IF NOT EXISTS T_Test(User NVARCHAR,Pass NVARCHAR);");
        mydb.execSQL("DROP TABLE IF EXISTS T_Test2;");
        mydb.execSQL("CREATE TABLE IF NOT EXISTS T_Test2(User NVARCHAR PRIMARY KEY ON CONFLICT REPLACE," +
                "Pass NVARCHAR);");
        mydb.execSQL("INSERT INTO T_Test VALUES('admin','admin');");


        mydb.execSQL("INSERT INTO T_Test2 VALUES('admin','admin');");

        Cursor res = mydb.rawQuery("Select * from T_Test2",null);
        res.moveToFirst();
        String username = res.getString(0);
        String password = res.getString(1);
        int Count = res.getCount();
        res.close();

        tv.setText("User : "+username+"\n"+"Pass : "+password+"\n Count : "+Count);

    }
}
